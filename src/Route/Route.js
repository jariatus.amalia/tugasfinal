import React, {useEffect, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {useSelector} from 'react-redux';
import Home from '../Home/Home';
import Login from '../Login/Login';
import Profile from '../Home/Profile';
import Regis from '../Login/Regis';
import AsyncStorage from '@react-native-async-storage/async-storage';
import History from '../Home/History';
import Transfer from '../Home/Transfer';
import Pulsa from '../Home/Pulsa';

const AppStack = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="History"
        component={History}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Transfer"
        component={Transfer}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Pulsa"
        component={Pulsa}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const AuthStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Regis"
        component={Regis}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const Stack = createNativeStackNavigator();
const Route = () => {
  const [tokens, setTokens] = useState(); // ini untuk simpan data dari async storage
  const ubah = useSelector(state => state.auth.AuthData); // nyimpan global data disini dari action

  const funcs = async () => {
    const token = await AsyncStorage.getItem('Token'); // ambil token
    setTokens(JSON.parse(token)); // masukan token ke usestate
    console.log('Tokens', JSON.parse(token));
  };

  useEffect(() => {
    funcs(); // function yang di jalankan
  }, [ubah]); // saat data berubah makan akan menjalakan function funcs
  console.log(ubah, 'ini lo2');

  console.log(tokens, 'ini kapan');

  return (
    <NavigationContainer>
      {/* jika token isinya tidak sama dengan null maka ? dia ke Home jika tidak dia ke Login */}
      {tokens ? <AppStack /> : <AuthStack />}
    </NavigationContainer>
  );
};

export default Route;
